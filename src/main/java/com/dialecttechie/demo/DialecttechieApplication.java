package com.dialecttechie.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DialecttechieApplication {

	public static void main(String[] args) {
		SpringApplication.run(DialecttechieApplication.class, args);
	}
}
