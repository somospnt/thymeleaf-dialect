package com.dialecttechie.demo;

import java.util.Arrays;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 *
 * @author usuario
 */
@Controller
public class HomeController {

    @GetMapping("/prueba")
    public String prueba(Model model) {
        Auto autoTincho = new Auto();
        autoTincho.setDuenio("Tincho");
        autoTincho.setPatente("OTH715");
        Auto autoColo = new Auto();
        autoColo.setDuenio("Colo");
        Auto autoSinDuenio = new Auto();
        autoSinDuenio.setPatente("OTH725");
        List<Auto> autos = Arrays.asList(autoTincho, autoColo, autoSinDuenio);
        model.addAttribute("autos", autos);
        return "home";
    }
}
